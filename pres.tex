\documentclass[aspectratio=169]{beamer}
\usepackage{bm}
\usepackage[utf8]{inputenc}
\usepackage{color}
\usepackage{multicol}
\usepackage{siunitx} %pretty measurement unit rendering
\usepackage[autoprint=false, gobble=auto]{pythontex} % create figures on-line directly from python!
\usepackage{hyperref} %enable hyperlink for urls

\usefonttheme[onlymath]{serif}
\setcounter{MaxMatrixCols}{20}

\DeclareSIUnit\pixel{px}

\usecolortheme[RGB={199,199,199}]{structure}
\usetheme{Dresden}

\newenvironment{figurehere}
{\def\@captype{figure}}
{}
\makeatother

\definecolor{slg}{gray}{0.30}
\definecolor{lg}{gray}{0.60}
\definecolor{vlg}{gray}{0.78}

\setbeamercolor{caption name}{fg=vlg}
\setbeamercolor{caption}{fg=vlg}
\setbeamertemplate{caption}{\raggedright\insertcaption\par}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{bibliography item}[text]

\input{pythontex/functions.tex}

\title{Education: General Linear Model}
\author{Horea Christian}
\institute{Institute for Biomedical Engineering, ETH and University of Zürich}
\begin{document}
	\input{pythontex/pycode.tex}
	\begin{frame}
		\titlepage
	\end{frame}
	\section{Basics}
		\subsection{Many formulations of the GLM}
			\begin{frame}{The General Expression}
				\centering \bm{$Y = XB + U$}
				\vspace{0.7cm}
				\begin{itemize}
					\item \bm{$Y$} = measurement matrix
					\item \bm{$X$} = (usually) design matrix
					\item \bm{$B$} = (usually) parameters to be estimated
					\item \bm{$U$} = error terms
				\end{itemize}
			\end{frame}
			\begin{frame}{A Simple Example}
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					0&1\\
					0&1\\
					0&1\\
					1&0\\
					1&0\\
					1&0
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\mu_1\\
					\mu_2\\
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
			\end{frame}
			\begin{frame}{One-Way ANOVA}
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					1&0&0\\
					1&0&0\\
					0&1&0\\
					0&1&0\\
					0&0&1\\
					0&0&1
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\mu_1\\
					\mu_2\\
					\mu_3\\
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
				\vspace{0.6cm}\\
				\centering $F = \frac{\text{variance between groups}}{\text{variance within groups}}$
				\vspace{0.3cm}\\
				Ombibus test for $H_0$: \textit{“The population means are equal.”}
			\end{frame}
			\begin{frame}{Two-Way ANOVA}
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					\textcolor{blue}{1}&0&0&\textcolor{red}{1}&0\\
					\textcolor{blue}{1}&0&0&0&\textcolor{red}{1}\\
					0&\textcolor{blue}{1}&0&\textcolor{red}{1}&0\\
					0&\textcolor{blue}{1}&0&0&\textcolor{red}{1}\\
					0&0&\textcolor{blue}{1}&\textcolor{red}{1}&0\\
					0&0&\textcolor{blue}{1}&0&\textcolor{red}{1}
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\alpha_1\\
					\alpha_2\\
					\alpha_3\\
					\beta_1\\
					\beta_2
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
				\vspace{1cm}\\
				\centering No interactions!
			\end{frame}
			\begin{frame}{Two-Way ANOVA}
				\scriptsize$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					\textcolor{blue}{1}&0&0&\textcolor{red}{1}&0&\textcolor{violet}{1}&0&0&0&0&0\\
					\textcolor{blue}{1}&0&0&0&\textcolor{red}{1}&0&\textcolor{violet}{1}&0&0&0&0\\
					0&\textcolor{blue}{1}&0&\textcolor{red}{1}&0&0&0&\textcolor{violet}{1}&0&0&0\\
					0&\textcolor{blue}{1}&0&0&\textcolor{red}{1}&0&0&0&\textcolor{violet}{1}&0&0\\
					0&0&\textcolor{blue}{1}&\textcolor{red}{1}&0&0&0&0&0&\textcolor{violet}{1}&0\\
					0&0&\textcolor{blue}{1}&0&\textcolor{red}{1}&0&0&0&0&0&\textcolor{violet}{1}
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\alpha_1\\
					\alpha_2\\
					\alpha_3\\
					\beta_1\\
					\beta_2\\
					\gamma_{11}\\
					\gamma_{12}\\
					\gamma_{21}\\
					\gamma_{22}\\
					\gamma_{31}\\
					\gamma_{32}\\
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
				\vspace{1cm}\\
				\centering With interactions...
			\end{frame}
			\begin{frame}{Simple Regression}
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					1&x_1\\
					1&x_2\\
					1&x_3\\
					1&x_4\\
					1&x_5\\
					1&x_6
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\beta_0\\
					\beta_1
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
				\vspace{0.3cm}\\
				Per-measurement expression: $y_i = \beta_0 + \beta_1 x_i + \epsilon_i$
			\end{frame}
			\begin{frame}{Multiple Regression}
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					1&x_1&w_1\\
					1&x_2&w_2\\
					1&x_3&w_3\\
					1&x_4&w_4\\
					1&x_5&w_5\\
					1&x_6&w_6
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\beta_0\\
					\beta_1\\
					\beta_2
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
				\vspace{0.3cm}\\
				Per-measurement expression: $y_i = \beta_0 + \beta_1 x_i + \beta_2 w_i + \epsilon_i$
			\end{frame}
	\section{SPM}
		\subsection{Statistical Parametric Mapping}
			\begin{frame}{Principle}
				\begin{itemize}
					\item \textbf{Statistical approach} // Software package
					\item Voxel-wise time-series analysis
					\item Mass-univariate analysis
				\end{itemize}
				\py[map]{fig}
			\end{frame}
			\begin{frame}{Single Voxel Regression Model - Basic Design}
				\vspace{0.52em}
				\noindent\makebox[\textwidth][c]{%
					\begin{minipage}{.15\textwidth}
						\py[data]{fig}
					\end{minipage}
					$= \beta_0 +$
					\begin{minipage}{.15\textwidth}
						\py[events]{fig}
					\end{minipage}
					$\times \beta_1 + \epsilon$
				}
			\end{frame}
			\begin{frame}{Single Voxel Regression Model - Convolution}
				\vspace{0.52em}
				\noindent\makebox[\textwidth][c]{%
					\begin{minipage}{.15\textwidth}
						\py[data]{fig}
					\end{minipage}
					$= \beta_0 +$
					\begin{minipage}{.15\textwidth}
						\py[design]{fig}
					\end{minipage}
					$\times \beta_1 + \epsilon$
				}
			\end{frame}
			\begin{frame}{Single Voxel Regression Model - Convolution, Multiple Regression}
				\vspace{0.52em}
				\noindent\makebox[\textwidth][c]{%
					\begin{minipage}{.15\textwidth}
						\py[data]{fig}
					\end{minipage}
					$= \beta_0 +$
					\begin{minipage}{.15\textwidth}
						\py[design]{fig}
					\end{minipage}
					$\times \beta_1 +$
					\begin{minipage}{.15\textwidth}
						\py[design_]{fig}
					\end{minipage}
					$\times \beta_2 + \epsilon$
				}
			\end{frame}
			\begin{frame}{In-Model “highpass filtering”}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.28]{img/nr.pdf}}
					\caption{ZNZ Lecture II., Christian Ruff}
				\end{figure}
				\begin{itemize}
					\item Low frequencies modeled via discrete cosines
				\end{itemize}
			\end{frame}
	\section{Application Issues}
		\subsection{More advanced considerents}
			\begin{frame}{Significance Thresholding}
				\begin{center}
					\textcolor{lg}{Multiple Comparison problem:}

					$\{H_{0,0}, ..., H_{0,99}\}  \Rightarrow$ 5 rejections with $p\leq0.05$
				\end{center}
				\begin{itemize}
					\item FWER - Bonferroni correction:\\
					\begin{center}\scriptsize \textcolor{lg}{$\alpha = $ probability of rejecting at least one true $H_{0,i}$}\end{center}
					\begin{center} $p_i \leq\frac{\alpha}{m}$ ; e.g. $\frac{0.05}{100} = 0.0005$\end{center}
					\item FDR - e.g. Benjamini–Hochberg procedure:\\
					\begin{center}\scriptsize \textcolor{lg}{$\alpha = $ proportion of rejected $H_{0,i}$ which were true}\end{center}
					\begin{center} $p_i \leq\frac{\alpha(m+1)}{2m}$ ; e.g. $\frac{0.05(100+1)}{2\cdot100} = 0.02525$\end{center}
				\end{itemize}
			\end{frame}
			\begin{frame}{Gaussian Random Field}
				\begin{itemize}
					\item Voxels are not statistically independent.
					\item 2-D autocorrenation.
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[scale=0.33]{img/grft.pdf}}
					\caption{ZNZ Lecture II., Christian Ruff}
				\end{figure}
			\end{frame}
			\begin{frame}{HRF Variability}
				\begin{itemize}
					\item HRF varies across time, coordinates, and participants
					\item \textcolor{lg}{Solution:} Multiple basis functions
				\end{itemize}
				\begin{figure}[htp] \centering{
					\includegraphics[width=0.45\textwidth]{img/bf.png}}
					\caption{Methods and models for fMRI data analysis 2012, Klaas-Enno Stephan}
				\end{figure}
			\end{frame}
			\begin{frame}{Random Effects}
				\begin{itemize}
					\item Integrate noise structure into model
				\end{itemize}
				\vspace{0.4cm}
				\centering\bm{$Y = XB + ZU + E$}
				\vspace{0.4cm}
				\begin{itemize}
					\item \bm{$Y$} = measured values
					\item \bm{$X, Z$} = design matrices relating to B and U
					\item \bm{$B$} = vector of fixed effects
					\item \bm{$U$} = vector of random effects (not estimated)
					\item \bm{$E$} = vector of error terms
				\end{itemize}
			\end{frame}
			\begin{frame}{Temporal Autocorrelation}
			Points in tme series are not statistically independent.
				$$
				\begin{bmatrix}
					y_1\\
					y_2\\
					y_3\\
					y_4\\
					y_5\\
					y_6
				\end{bmatrix}
				=
				\begin{bmatrix}
					1&t_1\\
					1&t_2\\
					1&t_3\\
					1&t_4\\
					1&t_5\\
					1&t_6
				\end{bmatrix}
				\times
				\begin{bmatrix}
					\beta_0\\
					\beta_1
				\end{bmatrix}
				+
				\begin{bmatrix}
					\epsilon_1\\
					\epsilon_2\\
					\epsilon_3\\
					\epsilon_4\\
					\epsilon_5\\
					\epsilon_6
				\end{bmatrix}
				$$
			\end{frame}
	\section{Further Info}
		\subsection{potentially useful consideations}
			\begin{frame}{General\textbf{ized} Linear Model}
				\begin{itemize}
					\item A number of common measurements (e.g. Reaction Times) are \textbf{not} normally distributed.
				\end{itemize}
				\vspace{0.5cm}
				\centering\bm{$\mathbb{E}(Y) = g^{-i}(X\beta)$}
				\vspace{0.3cm}
				\begin{itemize}
					\item \bm{$\mathbb{E}(Y)$} = expected value for Y
					\item \bm{$X\beta$} = linear predictor
					\item \bm{$g$} = “link function” (can combine multiple basis probability distributions)
				\end{itemize}
			\end{frame}
			\begin{frame}{Recommended Software}
				\begin{itemize}
					\item R
					\begin{itemize}
						\item https://cran.r-project.org/web/packages/nlme/index.html
						\item https://github.com/lme4/lme4/
					\end{itemize}
					\item Python
					\begin{itemize}
						\item http://docs.scipy.org/doc/scipy/reference/stats.html
						\item http://scikit-learn.org/stable/modules/linear\_model.html
					\end{itemize}
					\item FSL
					\begin{itemize}
						\item https://fsl.fmrib.ox.ac.uk/fsl/fslwiki
					\end{itemize}
				\end{itemize}
			\end{frame}
\end{document}
